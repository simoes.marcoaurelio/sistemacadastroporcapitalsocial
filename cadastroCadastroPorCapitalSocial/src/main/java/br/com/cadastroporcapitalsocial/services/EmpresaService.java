package br.com.cadastroporcapitalsocial.services;

import br.com.cadastroporcapitalsocial.producers.Empresa;
import br.com.cadastroporcapitalsocial.producers.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaProducer empresaProducer;

    public Empresa cadastrarEmpresa(Empresa empresa) {
        empresaProducer.enviarAoKafka(empresa);
        return empresa;
    }
}
