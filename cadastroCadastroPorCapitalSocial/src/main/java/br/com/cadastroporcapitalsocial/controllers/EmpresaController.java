package br.com.cadastroporcapitalsocial.controllers;

import br.com.cadastroporcapitalsocial.producers.Empresa;
import br.com.cadastroporcapitalsocial.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cadastro")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Empresa cadastrarEmpresa(@RequestBody @Valid Empresa empresa) {
        return empresaService.cadastrarEmpresa(empresa);
    }
}
