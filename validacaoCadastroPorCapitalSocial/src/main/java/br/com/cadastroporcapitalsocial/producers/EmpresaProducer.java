package br.com.cadastroporcapitalsocial.producers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaProducer {

    @Autowired
    private KafkaTemplate<String, CadastroEmpresa> producer;

    public void enviarAoKafka(CadastroEmpresa cadastroEmpresa) {
        producer.send("spec3-marco-aurelio-3", cadastroEmpresa);
    }
}
