package br.com.cadastroporcapitalsocial.producers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

public class CadastroEmpresa {

    private String cnpj;
    private BigDecimal capitalSocial;
    private LocalDate dataCadastro;
    private LocalTime horaCadastro;
    private boolean cadastroPermitido;

    public CadastroEmpresa() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(BigDecimal capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public LocalTime getHoraCadastro() {
        return horaCadastro;
    }

    public void setHoraCadastro(LocalTime horaCadastro) {
        this.horaCadastro = horaCadastro;
    }

    public boolean isCadastroPermitido() {
        return cadastroPermitido;
    }

    public void setCadastroPermitido(boolean cadastroPermitido) {
        this.cadastroPermitido = cadastroPermitido;
    }
}
