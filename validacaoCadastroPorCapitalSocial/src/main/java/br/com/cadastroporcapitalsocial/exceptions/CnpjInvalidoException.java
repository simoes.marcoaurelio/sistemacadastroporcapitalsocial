package br.com.cadastroporcapitalsocial.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class CnpjInvalidoException extends RuntimeException {
}
