package br.com.cadastroporcapitalsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ValidacaoCadastroPorCapitalSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidacaoCadastroPorCapitalSocialApplication.class, args);
	}
}
