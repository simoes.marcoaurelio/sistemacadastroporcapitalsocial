package br.com.cadastroporcapitalsocial.consumers;

import br.com.cadastroporcapitalsocial.dtos.RespostaReceita;
import br.com.cadastroporcapitalsocial.producers.CadastroEmpresa;
import br.com.cadastroporcapitalsocial.producers.Empresa;
import br.com.cadastroporcapitalsocial.producers.EmpresaProducer;
import br.com.cadastroporcapitalsocial.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    EmpresaService empresaService;

    @Autowired
    EmpresaProducer empresaProducer;

    @KafkaListener(topics = "spec3-marco-aurelio-2", groupId = "marcosimoes-1")
    public void receberDoKafka(@Payload Empresa empresa) {
        System.out.println("marcosimoes: Recebi o CNPJ " + empresa.getCnpj() + " para validação");

        RespostaReceita respostaReceita = empresaService.consultarCapitalSocial(empresa);

        CadastroEmpresa cadastroEmpresa = empresaService.preencherCadastro(respostaReceita);

        empresaProducer.enviarAoKafka(cadastroEmpresa);
    }
}
