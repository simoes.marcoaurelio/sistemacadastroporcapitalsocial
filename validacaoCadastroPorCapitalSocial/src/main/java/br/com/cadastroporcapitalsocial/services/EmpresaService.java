package br.com.cadastroporcapitalsocial.services;

import br.com.cadastroporcapitalsocial.clients.ReceitaClient;
import br.com.cadastroporcapitalsocial.dtos.RespostaReceita;
import br.com.cadastroporcapitalsocial.exceptions.CnpjInvalidoException;
import br.com.cadastroporcapitalsocial.producers.CadastroEmpresa;
import br.com.cadastroporcapitalsocial.producers.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@Service
public class EmpresaService {

    @Autowired
    private ReceitaClient receitaClient;

    public RespostaReceita consultarCapitalSocial(Empresa empresa) {

        Optional<RespostaReceita> optionalRespostaReceita = receitaClient.consultaCNPJ(empresa.getCnpj());
        RespostaReceita respostaReceita = optionalRespostaReceita.get();

        if (respostaReceita.getStatus().equals("OK")) {
            System.out.println("marcosimoes: A empresa " + respostaReceita.getCnpj() +
                    " tem o capital social de R$" + respostaReceita.getCapital_social());
            return respostaReceita;
        }
        else {
            throw new CnpjInvalidoException();
        }
    }

    public CadastroEmpresa preencherCadastro(RespostaReceita respostaReceita) {
        BigDecimal valorDoFiltro = new BigDecimal("1000000.00");
        CadastroEmpresa cadastroEmpresa = new CadastroEmpresa();

        if (respostaReceita.getCapital_social().compareTo(valorDoFiltro) > 0) {
            cadastroEmpresa.setCadastroPermitido(true);
        }
        else {
            cadastroEmpresa.setCadastroPermitido(false);
        }

        cadastroEmpresa.setCnpj(respostaReceita.getCnpj());
        cadastroEmpresa.setCapitalSocial(respostaReceita.getCapital_social());
        cadastroEmpresa.setDataCadastro(LocalDate.now());
        cadastroEmpresa.setHoraCadastro(LocalTime.now());

        return cadastroEmpresa;
    }
}
