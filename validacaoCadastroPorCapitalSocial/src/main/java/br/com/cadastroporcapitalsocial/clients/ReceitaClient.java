package br.com.cadastroporcapitalsocial.clients;

import br.com.cadastroporcapitalsocial.dtos.RespostaReceita;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "receitaws", url = "https://www.receitaws.com.br/v1/cnpj",
        configuration = ReceitaClientConfiguration.class)
public interface ReceitaClient {

    @GetMapping("/{cnpj}")
    Optional<RespostaReceita> consultaCNPJ(@PathVariable String cnpj);
}
