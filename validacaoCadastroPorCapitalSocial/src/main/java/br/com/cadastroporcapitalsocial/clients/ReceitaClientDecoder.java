package br.com.cadastroporcapitalsocial.clients;

import br.com.cadastroporcapitalsocial.exceptions.ProblemaAcessoReceitaException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ReceitaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        throw new ProblemaAcessoReceitaException();
    }
}
