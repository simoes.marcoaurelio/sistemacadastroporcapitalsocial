package br.com.cadastroporcapitalsocial.dtos;

import java.math.BigDecimal;

public class RespostaReceita {

    public String status;
    public String cnpj;
    public BigDecimal capital_social;

    public RespostaReceita() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(BigDecimal capital_social) {
        this.capital_social = capital_social;
    }
}
