package br.com.cadastroporcapitalsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZuulCadastroPorCapitalSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulCadastroPorCapitalSocialApplication.class, args);
	}

}
