package br.com.cadastroporcapitalsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServerCadastroPorCapitalSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServerCadastroPorCapitalSocialApplication.class, args);
	}

}
