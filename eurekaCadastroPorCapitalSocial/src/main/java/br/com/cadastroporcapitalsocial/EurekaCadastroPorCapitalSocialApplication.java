package br.com.cadastroporcapitalsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaCadastroPorCapitalSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaCadastroPorCapitalSocialApplication.class, args);
	}
}
