package br.com.cadastroporcapitalsocial.consumers;

import br.com.cadastroporcapitalsocial.producers.CadastroEmpresa;
import br.com.cadastroporcapitalsocial.services.CadastroEmpresaService;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CadastroEmpresaConsumer {

    @Autowired
    private CadastroEmpresaService cadastroEmpresaService;

    @KafkaListener(topics = "spec3-marco-aurelio-3", groupId = "marcosimoes-1")
    public void receberDoKafka(@Payload CadastroEmpresa cadastroEmpresa)
            throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        System.out.println("Recebi o CNPJ" + cadastroEmpresa.getCnpj() + " para cadastro");

        cadastroEmpresaService.montarArquivoCSV(cadastroEmpresa);
    }
}
