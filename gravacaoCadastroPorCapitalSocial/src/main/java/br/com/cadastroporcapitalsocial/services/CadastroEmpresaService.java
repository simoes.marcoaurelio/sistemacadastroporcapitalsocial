package br.com.cadastroporcapitalsocial.services;

import br.com.cadastroporcapitalsocial.producers.CadastroEmpresa;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Service
public class CadastroEmpresaService {

    public void montarArquivoCSV(CadastroEmpresa cadastroEmpresa)
            throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer = new FileWriter(
                "/home/a2/Documentos/workspace/sistemaCadastroPorCapitalSocial/arquivoDeCadastro/Cadastro.csv",
                true );
        StatefulBeanToCsv<CadastroEmpresa> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(cadastroEmpresa);

        writer.flush();
        writer.close();
    }
}
