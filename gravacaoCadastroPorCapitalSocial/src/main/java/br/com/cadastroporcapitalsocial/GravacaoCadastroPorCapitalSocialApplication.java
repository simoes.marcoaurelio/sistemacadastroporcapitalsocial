package br.com.cadastroporcapitalsocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GravacaoCadastroPorCapitalSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(GravacaoCadastroPorCapitalSocialApplication.class, args);
	}
}
